using UnityEngine;
using System.Collections;

public class GM: MonoBehaviour {
	
	public GameObject player;
	
	private GC cam;
	
	void Start () {
		cam = GetComponent<GC>();
		SpawnPlayer();
	}
	
	// Spawn player
	private void SpawnPlayer() {
		cam.SetTarget((Instantiate(player,Vector3.zero,Quaternion.identity) as GameObject).transform);
	}
	
	
	
}
