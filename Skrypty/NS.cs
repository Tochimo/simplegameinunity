using UnityEngine;
using System.Collections;

[RequireComponent(typeof(N))]
public class NS : MonoBehaviour {
	
	// Player Handling
	public float gravity = 20;
	public float speed = 8;
	public float acceleration = 30;
	public float jumpHeight = 12;
	
	private float currentSpeed;
	private float targetSpeed;
	private Vector2 amountToMove;
	
	private N zmiana;
	
	
	void Start () {
		zmiana = GetComponent<N>(); // wymóg kompenoentu
	}
	
	void Update () {
		// Reset acceleration upon collision
		
		
		if (zmiana.movementStopped) {
			targetSpeed = 0;
			currentSpeed = 0;
		}
		
		
		// If player is touching the ground
		if (zmiana.grounded) {
			amountToMove.y = 0;
			
// Skok używamy strzałki

/* 
			if(Input.GetKey(KeyCode.UpArrow))
			{
				amountToMove.y = jumpHeight;
			}

*/
			//Skok używamy domyśłnie spacja
			if (Input.GetButtonDown("Jump")) {
				amountToMove.y = jumpHeight;	
			}
			
		}
		
		
		// Input
		targetSpeed = Input.GetAxisRaw("Horizontal") * speed;
		currentSpeed = IncrementTowards(currentSpeed, targetSpeed,acceleration);
		
		// Set amount to move
		
		if (Input.GetKey(KeyCode.A))
		{
			transform.position += Vector3.left * speed * Time.deltaTime;
		}
		
		if (Input.GetKey(KeyCode.D))
		{
			transform.position += Vector3.right * speed * Time.deltaTime;
		}
		
		
		//amountToMove.x = currentSpeed;
		amountToMove.y -= gravity * Time.deltaTime;
		zmiana.Move(amountToMove * Time.deltaTime);
	}
	
	
	
	
	
	
	// Increase n towards target by speed
	private float IncrementTowards(float n, float target, float a) {
		if (n == target) {
			return n;	
		}
		else {
			float dir = Mathf.Sign(target - n); // must n be increased or decreased to get closer to target
			n += a * Time.deltaTime * dir;
			return (dir == Mathf.Sign(target-n))? n: target; // if n has now passed target then return target, otherwise return n
		}
	}
}
